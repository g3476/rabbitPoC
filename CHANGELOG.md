# Changelog


## 0.1.3 (2024-02-18)

### fix

* [d9631] d9631 [d9631](d9631)  [d9631341edcc4f0eb88ce32b9a986341a89f4370] fix: nyx fiddling (Petr Sykora)

# Changelog


## 0.1.2 (2024-02-18)

### fix

* [3a4a1] fix: nyx fiddling (Petr Sykora)

# Changelog


## 0.1.1 (2024-02-18)

### fix

* [] fix: PLS-123: nyx config\n\nref: PLS-234 (Petr Sykora)

