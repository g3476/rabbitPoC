package cz.glubo

import io.micronaut.security.authentication.ServerAuthentication
import java.security.Principal

fun Principal.getUserName(): String =
    when (this) {
        is ServerAuthentication ->
            this.attributes.getOrDefault(
                "preferred_username",
                this.name,
            ).toString()

        else -> this.name
    }

fun Principal.getUserId(): String =
    when (this) {
        is ServerAuthentication ->
            this.attributes.getOrDefault(
                "sub",
                this.name,
            ).toString()

        else -> this.name
    }

fun Principal.getIssuer(): String =
    when (this) {
        is ServerAuthentication ->
            this.attributes.getOrDefault(
                "iss",
                "Unknown Issuer",
            ).toString()

        else -> "Unknown Issuer"
    }
