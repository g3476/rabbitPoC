package cz.glubo

import io.micronaut.context.propagation.slf4j.MdcPropagationContext
import io.micronaut.core.propagation.MutablePropagatedContext
import io.micronaut.http.HttpRequest
import io.micronaut.http.annotation.Filter.MATCH_ALL_PATTERN
import io.micronaut.http.annotation.RequestFilter
import io.micronaut.http.annotation.ServerFilter
import org.slf4j.MDC

@ServerFilter(MATCH_ALL_PATTERN)
class LoggedInUserMDC {
    @RequestFilter
    fun handleRequest(
        request: HttpRequest<*>,
        mutablePropagatedContext: MutablePropagatedContext,
    ) {
        try {
            val principal = request.userPrincipal.orElse(null)
            MDC.put("username", principal?.getUserName())
            MDC.put("userid", principal?.getUserId())
            MDC.put("issuer", principal?.getIssuer())
            mutablePropagatedContext.add(MdcPropagationContext())
        } finally {
            MDC.remove("username")
            MDC.remove("userid")
            MDC.remove("issuer")
        }
    }
}
